# Тестовое задание Калькулятор Pro.Finansy

## Развернуто на Сервере по этому адресу

```
Grafana: http://194.163.150.187:3000/d/dLsDQIUnz/pro-finansy-calculator-app-metrics?orgId=1&refresh=5s

Swagger: http://194.163.150.187/:8000/docs
```

## Структура проекта

```
├── Dockerfile
├── README.md
├── app
│   ├── __init__.py
│   ├── api
│   │   ├── __init__.py
│   │   └── tasks.py
│   ├── db
│   │   ├── redis.py
│   │   └── redis_utils.py
│   ├── exceptions
│   │   └── tasks.py
│   ├── main.py
│   ├── schemas
│   │   ├── operators.py
│   │   └── tasks.py
│   ├── services
│   │   └── tasks.py
│   ├── settings.py
│   ├── tests
│   │   ├── __init__.py
│   │   ├── conftest.py
│   │   ├── test_celery.py
│   │   ├── test_health.py
│   │   └── test_tasks.py
│   ├── utils.py
│   └── workers
│       └── calculator_worker.py
├── docker-compose.yaml
├── etc
│   ├── dashboards
│   │   └── fastapi-observability.json
│   ├── dashboards.yaml
│   ├── grafana
│   │   └── datasource.yml
│   └── prometheus
│       └── prometheus.yml
├── poetry.lock
└── pyproject.toml
```


## Развернуть Проект Локально

Процес развертывания происходит через Dockerfile и docker-compose

Перед Этим установите плагин для Grafana для стабильной работы

```
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
```

Затем подымаем приложение

```
docker-compose up -d --build
```

## Документация

Для подробного изучения перейти на Swagger

```
http://localhost:8000/docs
```

## Запуск тестов

```
docker-compose exec app-calc pytest .  
```

## Просмотр метрик

Для просмотра метрик приложения нужно перейти в Grafana по адресу

```
http://localhost:3000/d/dLsDQIUnz/pro-finansy-calculator-app-metrics?orgId=1&refresh=5s
```

## Полезные Ссылки

- [FastApi Observability](https://github.com/blueswen/fastapi-observability)
- [FastApi Docs](https://fastapi.tiangolo.com/)
- [Celery FastApi Setup](https://testdriven.io/blog/fastapi-and-celery/)


***
