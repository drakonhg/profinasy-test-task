from fastapi import APIRouter

from app.api.tasks import tasks_router

api_router = APIRouter(prefix="/api")

api_router.include_router(tasks_router)
