import logging

from fastapi import APIRouter, Depends

from app.db.redis import get_redis_session
from app.schemas.tasks import TaskIdSchema, TaskInSchema, TaskResultSchema
from app.services.tasks import create_task, get_task_result, get_tasks_queue

tasks_router = APIRouter(prefix="/tasks")


@tasks_router.get("/", response_model=list[TaskResultSchema], status_code=200)
async def operation_results_list_handler(rd_session=Depends(get_redis_session)):
    logging.debug(f"Calling get tasks list")
    tasks = await get_tasks_queue(rd_session)
    return tasks


@tasks_router.get("/{task_id}", response_model=TaskResultSchema, status_code=200)
async def operation_result_handler(task_id: str):
    logging.debug(f"Calling get task with id: {task_id}")
    task_result = await get_task_result(task_id)
    return task_result


@tasks_router.post("/", response_model=TaskIdSchema, status_code=201)
async def operation_handler(data: TaskInSchema):
    logging.debug(
        f"Calling create task with params: {data.x} {data.operation} {data.y}"
    )
    task_id = await create_task(data)
    return task_id
