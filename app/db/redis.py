from aioredis import Redis


class RedisConnectionDB:
    conn: Redis


redis = RedisConnectionDB()


async def get_redis_session():
    async with redis.conn.client() as conn:
        yield conn
