import logging

import aioredis

from app.db.redis import redis
from app.settings import settings


async def connect_to_redis():
    logging.info(f"Connecting to redis...")
    redis.conn = aioredis.from_url(
        settings.redis_url, encoding="utf-8", decode_responses=True
    )
    logging.info(f"Connection to redis set successfully")


async def disconnect_from_redis():
    logging.info(f"Disconnecting from redis...")
    await redis.conn.close()
    logging.info(f"Disconnecting from redis successfully")
