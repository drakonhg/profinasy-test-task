from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND

TaskNotFound = HTTPException(
    status_code=HTTP_404_NOT_FOUND,
    detail="Task not found",
)
