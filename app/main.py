import logging

import uvicorn
from fastapi import FastAPI

from app.api import api_router
from app.db.redis_utils import connect_to_redis, disconnect_from_redis
from app.settings import settings
from app.utils import PrometheusMiddleware, metrics, setting_otlp

app = FastAPI()

app.include_router(api_router)
app.add_middleware(PrometheusMiddleware, app_name=settings.app_name)
app.add_route("/metrics", metrics)

setting_otlp(app, settings.app_name, settings.otlp_grpc_endpoint)


class EndpointFilter(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        return record.getMessage().find("GET /metrics") == -1


logging.getLogger("uvicorn.access").addFilter(EndpointFilter())


@app.on_event("startup")
async def startup_handler():
    await connect_to_redis()


@app.on_event("shutdown")
async def shutdown_handler():
    await disconnect_from_redis()


if __name__ == "__main__":
    log_config = uvicorn.config.LOGGING_CONFIG
    log_config["formatters"]["access"][
        "fmt"
    ] = "%(asctime)s %(levelname)s [%(name)s] [%(filename)s:%(lineno)d] [trace_id=%(otelTraceID)s span_id=%(otelSpanID)s resource.service.name=%(otelServiceName)s] - %(message)s"
    uvicorn.run(app, host="0.0.0.0", port=settings.app_port, log_config=log_config)
