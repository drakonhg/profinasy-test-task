from typing import Literal

OPERATORS = Literal["+", "-", "/", "*"]
