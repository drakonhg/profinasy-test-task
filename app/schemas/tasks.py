from pydantic import BaseModel

from app.schemas.operators import OPERATORS


class TaskInSchema(BaseModel):
    x: int | float
    y: int | float
    operation: OPERATORS


class TaskResultSchema(BaseModel):
    task_id: str
    result: int | float
    status: str


class TaskIdSchema(BaseModel):
    task_id: str
