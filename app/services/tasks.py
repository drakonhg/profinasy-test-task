import json
import logging

from aioredis import Redis
from celery.result import AsyncResult

from app.exceptions.tasks import TaskNotFound
from app.schemas.tasks import TaskIdSchema, TaskInSchema, TaskResultSchema
from app.workers.calculator_worker import calculate_numbers


async def get_tasks_queue(redis_conn: Redis) -> list[TaskResultSchema]:
    tasks: list[TaskResultSchema] = []
    async for key in redis_conn.scan_iter("celery-task-meta-*"):
        task: dict = json.loads(await redis_conn.get(key))
        tasks.append(TaskResultSchema(**task))
    return tasks


async def get_task_result(task_id: str) -> TaskResultSchema:
    try:
        task_result = AsyncResult(task_id)
        return TaskResultSchema(
            task_id=task_id, result=task_result.result, status=task_result.status
        )
    except Exception as e:
        logging.error(f"{e}")
        raise TaskNotFound


async def create_task(data: TaskInSchema) -> TaskIdSchema:
    task = calculate_numbers.delay(data.x, data.y, data.operation)
    return TaskIdSchema(task_id=task.id)
