import os

from pydantic import BaseSettings, Field, RedisDsn


class Settings(BaseSettings):
    app_name: str = os.environ.get("APP_NAME", "fastapi-app")
    app_port: int = os.environ.get("APP_PORT", 8000)
    redis_url: RedisDsn = os.environ.get("REDIS_URL", "redis://redis:6379/0")
    otlp_grpc_endpoint: str = os.environ.get("OTLP_GRPC_ENDPOINT", "http://tempo:4317")


settings = Settings()
