from app.workers.calculator_worker import calculate_numbers


def test_celery_worker_init(celery_app, celery_worker):
    assert True


def test_celery_tasks(celery_app, celery_worker):

    assert calculate_numbers.delay(2, 3, "+").get(timeout=1) == 5
    assert calculate_numbers.delay(3, 10, "*").get(timeout=1) == 30
    assert calculate_numbers.delay(5, 3, "-").get(timeout=1) == 2
    assert calculate_numbers.delay(5, 1, "/").get(timeout=1) == 5
