import json

from app.api import tasks
from app.main import app
from app.schemas.tasks import TaskIdSchema


def test_task_create(test_app, monkeypatch):
    test_request_payload = {"x": 10, "y": 65, "operation": "*"}
    test_response_payload = {"task_id": "f3b19a0c-20d7-441d-966f-13a4b53b476d"}

    async def mock_create_task(payload):
        return TaskIdSchema(task_id="f3b19a0c-20d7-441d-966f-13a4b53b476d")

    monkeypatch.setattr(tasks, "create_task", mock_create_task)

    resp = test_app.post("/api/tasks", content=json.dumps(test_request_payload))
    assert resp.status_code == 201
    assert resp.json() == test_response_payload


def test_get_task(test_app, monkeypatch):
    test_data = {
        "task_id": "eid1p43-aelc2-47e5-8bfc-97251d7997e6",
        "result": 21,
        "status": "SUCCESS",
    }

    async def mock_get(id):
        return test_data

    monkeypatch.setattr(tasks, "get_task_result", mock_get)

    resp = test_app.get("/api/tasks/eid1p43-aelc2-47e5-8bfc-97251d7997e6")
    assert resp.status_code == 200
    assert resp.json() == test_data


def test_get_tasks_list(test_app, monkeypatch, fastapi_dep):
    with fastapi_dep(app).override({"get_redis_session": lambda: print("Override dep")}):
        test_data = [
            {
                "task_id": "ceid1p43-aelc2-47e5-8bfc-97251d7997e6",
                "result": 21,
                "status": "SUCCESS",
            },
            {
                "task_id": "f3b19a0c-20d7-441d-966f-13a4b53b476d",
                "result": 650,
                "status": "SUCCESS",
            },
        ]

        async def mock_get_list(rd_mock):
            return test_data

        monkeypatch.setattr(tasks, "get_tasks_queue", mock_get_list)

        resp = test_app.get("/api/tasks")
        assert resp.status_code == 200
        assert resp.json() == test_data
