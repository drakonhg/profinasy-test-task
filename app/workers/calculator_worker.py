import logging

from celery import Celery

from app.schemas.operators import OPERATORS
from app.settings import settings

celery = Celery("calculations", broker=settings.redis_url, backend=settings.redis_url)


@celery.task(name="calculate_numbers")
def calculate_numbers(x: int, y: int, operator: OPERATORS):
    logging.info(f"Calculating numbers: {x} {operator} {y}")
    match operator:
        case "+":
            return x + y
        case "-":
            return x - y
        case "*":
            return x * y
        case "/":
            if y == 0:
                return 0
            return x / y
